# Вкратце

* 2 точки входа - app.ts и app-mel.ts
* фишки TypeScript - Наследование классов, импорт\экспорт модулей
* использование модуля JavaScript (JS) в файле с TypeScript (TS):
[super-bundler / src / helpers / ](https://bitbucket.org/ColCh/super-bundler/src/7cce776f3055/src/helpers/?at=webpack-typescript)
Тут нужно написать файл декларации модуля для TypeScript. 
* require для одинакового уровня вложенности : [ super-bundler / src / modules / HelloWorld.ts ](https://bitbucket.org/ColCh/super-bundler/src/7cce776f30555ff3b9dee73920f61d732bacabb6/src/modules/HelloWorld.ts?at=webpack-typescript) : 
  ```
  import EchoHelper = require('helpers/EchoHelper');
  ```
  включит модуль `super-bundler / src / helpers / EchoHelper` 
* неиспользуемые файлы не включаются ( [super-bundler / src / modules / UnusedModule.ts](https://bitbucket.org/ColCh/super-bundler/src/7cce776f30555ff3b9dee73920f61d732bacabb6/src/modules/UnusedModule.ts?at=webpack-typescript) )

## Подробнее

Настройки webpack написаны в Gruntfile.js

Куски кода - куски из конфига  в GruntFile.js

Из файла настройки:
```
entry: {
  "bundle": "./src/app.ts",
  "bundle-mel": "./src/app-mel.ts"
},
```

ключ ("bundle", "bundle-mel") - имена для бандлов.

они компилятся и складываются в каталог "public/dist". К имени бандла дописывается ".js"

```
 output: {
            path: "./public/dist",
            filename: "[name].js",
        },
```

Т.е. будет 2 файла на выходе - "./public/dist/bundle.js" и "./public/dist/bundle-mel.js"

По умолчанию папки для модулей - это web_modules и node_modules. В этом проекте модули лежат в src:

```
resolve: {
          modulesDirectories: ["src"],
        },
```

Теперь файлы проекта можно вызывать как обычные модули в nodejs


По умолчанию webpack работает только с JS файлами. Для поддержки других типов файлов пропишем, что TS файлы - это тоже модули

```
resolve: {
          extensions: ["", ".webpack.js", ".web.js", ".js", ".ts"]
        },
```

и укажем, какой загрузчик использовать для ts файлов:

```
module: {
          loaders: [
            { test: /\.ts$/, loader: "ts-loader" }
          ]
        },
```

Это всё