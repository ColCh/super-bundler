var path = require('path');

module.exports = function(grunt) {

  var sourceMap = false;

  grunt.initConfig({

    // Очистка каталога сборки
    clean: ['public'],

    webpack: {
      bundle: {

        devtool: sourceMap ? 'source-map':'',

        entry: {
          "bundle": "./src/app.ts",
          "bundle-mel": "./src/app-mel.ts"
        },

        output: {
            path: "./public/dist",
            filename: "[name].js",
        },

        module: {
          loaders: [
            { test: /\.ts$/, loader: "ts-loader?noResolve&module=commonjs&target=ES5" + (sourceMap ? "&sourcemap":"") }
          ]
        },

        resolve: {
          modulesDirectories: ["src"],
          extensions: ["", ".webpack.js", ".web.js", ".js", ".ts"]
        },

        stats: {
            colors: true,
            modules: true,
            reasons: true
        },

        failOnError: false
      }
    }

  })

  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-webpack');

  grunt.task.registerTask('default', ['clean', 'webpack:bundle'])

}