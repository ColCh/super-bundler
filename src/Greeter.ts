import HelloWorld = require('modules/HelloWorld');

class Greeter extends HelloWorld {

    constructor(name) {
        super();
        this.name = name;
    }

    get greeting () {
        return 'Hello, ' + this.name + '!';
    }

}

export = Greeter